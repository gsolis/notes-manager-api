const express = require('express');
const cors = require('express-cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const NotesResource = require('./resources/notes');
 
const app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(cors({
    allowedOrigins: [
        'http://localhost:3000'
    ]
}));

mongoose.connect('mongodb://luizorozco89:abc1989@ds139879.mlab.com:39879/db1', { useMongoClient: true });
const db = mongoose.connection;
db.on('error', (error) => {
    console.log('connection error: ', error);
});
db.once('open', () => {
    console.log('we\'re connected!');
});

NotesResource(app);

app.listen(3001, function () {
  console.log('App listening on port 3001!')
});
