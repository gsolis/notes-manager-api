const NotesController = require('../controllers/notes');

const NotesResource = (app) => {

    const notesController = new NotesController();

    app.get('/notes', (req, res) => {
        notesController.getNotes((error, notes) => {
            if (error) return res.send(500);;
            return res.send(notes);
        });
    });

    app.get('/notes/:id', (req, res) => {
        notesController.getNoteById(req.params.id, (error, note) =>{
            if (error) return res.send(500);;
            return res.send(note);
        });
    });

    app.post('/notes', (req, res) => {
        const newNote = req.body;
        notesController.createNote(newNote, (error, note) => {
            if (error) return res.send(500);
            res.send(note);
        });
    });

    app.put('/notes/:id', (req, res) => {
        const noteId = req.params.id,
            noteWithUpdates = req.body;

        notesController.updateNote(noteId, noteWithUpdates, (error, updatedNote) => {
            if (error) return res.send(500);
            res.send(updatedNote);
        });
    });

    app.delete('/notes/:id', (req, res) => {
        const noteId = req.params.id;

        notesController.deleteNote(noteId, (error) => {
            if (error) return res.send(500);
            res.send(200);
        }).exec();
    });

};

module.exports = NotesResource;