const mongoose = require('mongoose');

//Schema
const noteSchema = mongoose.Schema({
    title: String,
    description: String,
    status: String,
    date: Date
});

const Note = mongoose.model('Note', noteSchema);

module.exports = Note;