const Note = require('../models/note');

class NotesController {

    getNotes(callback) {
        return Note
            .find()
            .sort('-date')
            .find(callback);
    }

    getNoteById(id, callback) {
        return Note.findById(id, callback);
    }

    createNote(note, callback) {
        const newNote = Object.assign({}, note, { date: new Date() }),
            newNoteToDb = new Note(newNote);

        return newNoteToDb.save(callback);
    }

    updateNote(id, note, callback) {
        Note.findOneAndUpdate(
            { _id: id },
            { $set: note },
            { new: true },
            callback
        );
    }

    deleteNote(id, callback) {
        return Note.findByIdAndRemove(id, callback);
    }

}

module.exports = NotesController;